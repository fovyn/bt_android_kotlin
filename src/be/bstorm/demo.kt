package be.bstorm

import be.bstorm.models.Person

fun main(args: Array<String>) {
    val items: Array<Int> = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    items.mySort { i1, i2 -> i1 < i2 }

    val peoples = arrayOf(
        Person("John0", "Smith"),
        Person("John1", "Smith")
    )

    peoples.forEach { println(it) }
    mySort(peoples) { it1, it2 -> it1.lastName > it2.lastName }
    peoples.forEach { println(it) }
}

fun <T> mySort(items: Array<T>, predicate: (i1: T, i2: T) -> Boolean) {
    for (i in 0..<items.size - 1) {
        var iPredicate = i
        for (j in i + 1..<items.size) {
            if (!predicate(items[iPredicate], items[j])) {
                iPredicate = j
            }
        }
        if (iPredicate != i) {
            val tmp = items[i]
            items[i] = items[iPredicate]
            items[iPredicate] = tmp
        }
    }
}

fun <T> Array<T>.mySort(predicate: (i1: T, i2: T) -> Boolean) {
    for (i in 0..<this.size - 1) {
        var iPredicate = i
        for (j in i + 1..<this.size) {
            if (!predicate(this[iPredicate], this[j])) {
                iPredicate = j
            }
        }
        if (iPredicate != i) {
            val tmp = this[i]
            this[i] = this[iPredicate]
            this[iPredicate] = tmp
        }
    }
}

