package be.bstorm.models

interface ICustomer {
    val currency: Double
        get

    fun deposit(amount: Double)
    fun withdraw(amount: Double)
}