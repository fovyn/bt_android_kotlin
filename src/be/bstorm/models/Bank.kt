package be.bstorm.models

import be.bstorm.models.accounts.Account

class Bank(
    val name: String
) : AutoCloseable {
    private val _accounts = mutableMapOf<String, Account>()

    val accounts: List<Account>
        get() = _accounts.values.toList()

    operator fun get(number: String): Account? {
        return _accounts[number]
    }
//
//    operator fun set(number: String, account: Account) {
//        _accounts[number] = account
//    }

    fun add(account: Account) {
        if (_accounts.containsKey(account.number)) return
        _accounts[account.number] = account
    }

    fun remove(account: Account) {
        if (!_accounts.containsKey(account.number)) return

        remove(account.number)
    }

    fun remove(number: String) {
        if (!_accounts.containsKey(number)) return

        _accounts.remove(number)
    }

    fun totalAccounts(owner: Person) {
        var total = 0.0

        val accounts = mutableListOf<Account>()

        _accounts.forEach { (number, account) ->
            {
                if (account.owner == owner) {
                    accounts.add(account)
                }
            }
        }

        accounts.forEach { total = it + total }


//        val accounts = _accounts.values.stream()
//            .filter { p: Account -> p.owner == owner }
//            .mapToDouble { it.currency }
//            .sum()
    }

    override fun close() {
        TODO("Not yet implemented")
    }
}