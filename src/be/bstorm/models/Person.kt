package be.bstorm.models

import java.time.LocalDate

data class Person(
    val lastName: String,
    val firstName: String,
    val birthDate: LocalDate = LocalDate.of(1970, 1, 1),
)