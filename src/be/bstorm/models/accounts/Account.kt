package be.bstorm.models.accounts

import be.bstorm.models.AccountState
import be.bstorm.models.IBanker
import be.bstorm.models.ICustomer
import be.bstorm.models.Person
import java.security.InvalidParameterException

abstract class Account(
    _owner: Person,
    _number: String,
    _currency: Double = 0.0
) : ICustomer, IBanker {
    override val owner = _owner
    override val number = _number
    override var currency = _currency

    var state: AccountState = AccountState.OPEN

    class AccountBuilder() {
        lateinit var number: String
        lateinit var ownerFirstName: String
        lateinit var ownerLastName: String
        lateinit var accountType: String

        fun setNumber(number: String): AccountBuilder {
            this.number = number
            return this
        }

        fun setOwnerFirstName(ownerFirstName: String): AccountBuilder {
            this.ownerFirstName = ownerFirstName
            return this
        }

        fun setOwnerLastName(ownerLastName: String): AccountBuilder {
            this.ownerLastName = ownerLastName
            return this
        }

        fun setAccountType(accountType: String): AccountBuilder {
            this.accountType = accountType
            return this
        }

        fun build(): Account = when (accountType) {
            "current" -> CurrentAccount(Person(ownerLastName, ownerFirstName), "BE12 1234 1234 123" + ++CURRENT_ACCOUNT)
            else -> SavingAccount(Person(ownerLastName, ownerFirstName), "BE12 1234 1234 123" + ++CURRENT_ACCOUNT)
        }
    }

    companion object {
        var CURRENT_ACCOUNT = 1

        fun builder(): AccountBuilder {
            return AccountBuilder()
        }
    }

    protected abstract fun computeInterest(): Double
    override fun deposit(amount: Double) {
        if (amount < 0) throw InvalidParameterException("amount must be >= 0")
        currency += amount
    }

    override fun withdraw(amount: Double) {
        if (state == AccountState.CLOSE) {
        }
        if (amount < 0) throw InvalidParameterException("amount must be >= 0")
        currency -= amount
    }

    operator fun plus(account: Account): Double {
        return this.currency + account.currency
    }

    operator fun plus(currency: Double): Double {
        return this.currency + currency
    }

    override fun applyInterest() {
        this.currency += computeInterest()
    }
}