package be.bstorm.models.accounts

import be.bstorm.models.Person
import java.time.LocalDate

class SavingAccount(
    owner: Person,
    number: String,
    _currency: Double = 0.0
) : Account(owner, number, _currency) {
    private var _lastWithDrawDate = LocalDate.now()
    val lastWithDrawDate
        get() = LocalDate.from(_lastWithDrawDate)

    override fun computeInterest(): Double = currency * 0.045
    override fun withdraw(amount: Double) {
        if (currency - amount < 0) return
        super.withdraw(amount)
        _lastWithDrawDate = LocalDate.now()
    }
}