package be.bstorm.models.accounts

import be.bstorm.exceptions.CustomException.InvalidOperationException
import be.bstorm.exceptions.NotEnoughCurrencyException
import be.bstorm.models.Person

// flavian.ovyn@bstorm.be

// https://gitlab.com/fovyn/bt_android_kotlin

class CurrentAccount(
    owner: Person,
    number: String,
    _currency: Double = 0.0,
    private var _creditLine: Double = 0.0,
) : Account(owner, number, _currency) {
    var creditLine: Double
        get() = _creditLine
        set(value) {
            if (value >= 0.0) {
                _creditLine = value
            }
            throw InvalidOperationException(this)
        }

    //
    override fun withdraw(amount: Double) {
        if (currency - amount < -creditLine) throw NotEnoughCurrencyException(this)
        super.withdraw(amount)
    }

    protected override fun computeInterest(): Double = currency * if (currency > 0.0) 0.03 else 0.0975
}

//data class CurrentAccount(
//    override val owner: Person,
//    override val number: String,
//    override var _currency: Double = 0.0,
//    var _creditLine: Double = 0.0
//) : Account(owner, number, _currency) {
//    var creditLine: Double
//        get() = _creditLine
//        set(value) {
//            if (value >= 0.0) {
//                _creditLine = value
//            }
//        }
//
//    override fun withdraw(amount: Double) {
//        if (currency - amount < -creditLine) return
//
//        super.withdraw(amount)
//    }
//}