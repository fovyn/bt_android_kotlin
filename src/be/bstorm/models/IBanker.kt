package be.bstorm.models

interface IBanker : ICustomer {
    val owner: Person
    val number: String

    fun applyInterest()
}