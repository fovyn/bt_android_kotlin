package be.bstorm

class MyDoubleStream(val items: Collection<Double>) {
    fun sum(): Double {
        var total = 0.0

        for (item in items) {
            total += item
        }

        return total
    }
}

class MyStream<T>(val items: Collection<T>) {

    fun filter(predicate: (T) -> Boolean): MyStream<T> {
        val collection = mutableListOf<T>()

        for (item in items) {
            if (predicate(item)) {
                collection.add(item)
            }
        }

        return MyStream(collection)
    }

    fun mapToDouble(action: (T) -> Double): MyDoubleStream {
        val collection = mutableListOf<Double>()

        for (account in items) {
            collection.add(action(account))
        }

        return MyDoubleStream(collection)
    }
}