package be.bstorm

import be.bstorm.exceptions.CustomException
import be.bstorm.models.Bank
import be.bstorm.models.accounts.Account

object Ui {
    fun addAccount(): Account {
        val builder = Account.builder()

        print("Enter your first name: ")
        builder.setOwnerFirstName(readln())
        print("Enter your last name:")
        builder.setOwnerLastName(readln())
        print("Enter your birth date (yyyy-mm-dd):")
        val (year, month, day) = readln().split("-")
        print("Which account type ? (current, saving)")
        builder.setAccountType(readln())

        return builder.build()
    }
}

fun main(args: Array<String>) {
    val bank = Bank("PicsouBank")
    val account = Ui.addAccount()
    
    bank.add(account)
}