package be.bstorm.exceptions

import be.bstorm.models.accounts.Account

sealed class CustomException(message: String) : Exception(message) {
    class InvalidOperationException(account: Account) : CustomException("${account.number} is not an operation")
}