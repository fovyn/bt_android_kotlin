package be.bstorm.exceptions

import be.bstorm.models.accounts.Account

class NotEnoughCurrencyException(val account: Account) :
    Exception("Account numbered ${account.number} haven't enough currency") {
}